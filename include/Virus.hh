//Virus.hh

#ifndef VIRUS_HH_
#define VIRUS_HH_

#include "Life.hpp"
#include "Cell.hh"


class Virus : public Life
{
  //fonctions amies

public:
  //constructeurs

  //destructeur
  virtual ~Virus();

  //méthodes statiques et swap

  //accesseurs

  //mutateurs

  //méthodes

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  Cell* _host;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //VIRUS_HH_
