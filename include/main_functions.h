//main_functions.h

/*
*Ce header définie toutes les conctions pouvant
*être appelées depuis la fonction main.
*/

#ifndef MAIN_FUNCTIONS_H_
#define MAIN_FUNCTIONS_H_

#include <iostream>
#include <unordered_set>
#include <string>

void clearConsole(unsigned lines = 80);
void version();
void help();
void invalideArgs(std::unordered_set<std::string> & args);
void mainPlay();
void mainNoGUIPlay();
void mainTest();

#endif //MAIN_FUNCTIONS_H_
