//jsonHandler.h

#ifndef JSONHANDLER_H_
#define JSONHANDLER_H_

#include "json.hh"
#include <string>


std::string getKeyBracket(std::string const& key);
std::string getKeyBracket(unsigned key);
nlohmann::json loadFromJson(nlohmann::json const& j, std::string const& key, std::string const& nodes);
nlohmann::json loadFromJson(nlohmann::json const& j, unsigned key, std::string const& nodes);

#endif //JSONHANDLER_H_
