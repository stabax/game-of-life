//Placeable.hpp

/*
*Cette interface représente tous
*les éléments se trouvant nécéssairement
*sur un slot.
*/

#ifndef PLACEABLE_HPP_
#define PLACEABLE_HPP_

#include <memory>

#include "Point.hh"
#include "Identifiable.hpp"

class Slot;

class Placeable : public Identifiable
{
  //raison de changement : Aucune

  //fonctions amies

public:
  //constructeurs
  Placeable(std::string const& id, std::weak_ptr<Slot> const& slot);

  //destructeur
  virtual ~Placeable() = 0;

  //méthodes statiques et swap

  //accesseurs
  Point getPosition() const;

  //mutateurs

  //méthodes

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  std::weak_ptr<Slot> _slot;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //PLACEABLE_HPP_
