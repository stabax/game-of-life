//Protein.hh

/*
*Cette classe représente une proteine,
*element pouvant être produite uniquement par des Lifes.
*Elles permettent d'acceder a des reactions chimiques (enzyme),
*de communiquer entre cellules (hormonnes/recepteurs),
*de structurer la cellule (proteine struturelle),
*d'attaquer (toxines) ...
*/

#ifndef PROTEIN_HH_
#define PROTEIN_HH_

#include <unordered_map>
#include <unordered_set>

#include "Molecule.hh"


class Protein : public Molecule
{
  //fonctions amies
  friend bool operator==(Protein const& Obj1, Protein const& Obj2);
  friend bool operator!=(Protein const& Obj1, Protein const& Obj2);
  friend std::hash<Protein>;

public:

  //constructeurs
  Protein(std::string const& name, unsigned usage = 0);

  //destructeur
  virtual ~Protein();

  //méthodes statiques et swap
  static std::unordered_map<std::string, unsigned> loadEffects(std::string const& name);
  static void loadProteinFile(std::string const& path);

  //accesseurs

  //mutateurs

  //méthodes
  virtual std::string checkList(std::string const& name);

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  unsigned _maxUsage;
  unsigned _usage;
  std::unordered_map<std::string, unsigned> _effects;

  //tuple<0>=temperature de vaporisation
  //tuple<1>=nombre d'usage maximum
  static std::unordered_map<std::string, std::tuple<int, unsigned>> proteinList;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )
bool operator==(Protein const& Obj1, Protein const& Obj2);
bool operator!=(Protein const& Obj1, Protein const& Obj2);



namespace std
{
  template<>
  class hash<Protein>
  {
    public:
    size_t operator()(Protein const& Obj) const
    {
        return hash<string>()(Obj._name) ^ hash<unsigned>()(Obj._usage);
    }
  };
}


#endif //PROTEIN_HH_
