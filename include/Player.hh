//Player.hh

#ifndef PLAYER_HH_
#define PLAYER_HH_

#include <string>

#include "Point.hh"
#include "Terrain.hh"
#include "Identifiable.hpp"

class Player : public Identifiable

{
  //fonctions amies

public:
  //constructeurs
  Player(Player const& Ojb) = delete;

  //destructeur
  virtual ~Player() = 0;

  //méthodes statiques et swap

  //accesseurs

  //mutateurs

  //méthodes

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)
  Player& operator=(Player const& Ojb) = delete;

protected:
  //attributs
  std::string _id;
  std::string _pseudo;
  Terrain* _Terr;
  Point* _cursor;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //PLAYER_HH_
