//Terrain.hh

#ifndef TERRAIN_HH_
#define TERRAIN_HH_

#include <unordered_map>

#include "Point.hh"
#include "Slot.hh"
#include "json.hh"

class Terrain
{
  //fonctions amies
  friend class TerrainLoader;

public:
  //constructeurs
  Terrain();
  Terrain(int temperature,
          unsigned brightness,
          unsigned Xrays,
          Point const& dimensions,
          std::unordered_map<Point, std::shared_ptr<Slot>> terrain,
          std::unordered_map<Molecule, unsigned> gas);
  Terrain(std::string const& terrainPath);
  Terrain(Terrain const& Obj) = delete;

  //destructeur
  ~Terrain();

  //méthodes statiques et swap

  //accesseurs
  Point const& dimensions() const;
  std::tuple<int, unsigned, unsigned> const getProperties() const;
  std::unordered_map<Molecule, unsigned> const& getGas() const;

  //mutateurs

  //forward
  bool hasSlotMolecule(Point const& pos, unsigned amount) const;
  bool hasSlotProtein(Point const& pos, unsigned amount) const;
  bool hasSlotLife(Point const& pos, unsigned amount) const;

  //méthodes


  void decale(unsigned x, unsigned y); //décale les Placeable, sans les affecter (utile lorsqu'on agrandi le terrain)

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)
  Terrain& operator=(Terrain const& Obj) = delete;

protected:
  //attributs
  //cet attribut est nécéssaire pour les
  int _temperature;
  unsigned _brightness;
  unsigned _Xrays;
  Point _dimensions;
  std::unordered_map<Point, std::shared_ptr<Slot>> _terrain;
  std::unordered_map<Molecule, unsigned> _gas;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )

#endif //TERRAIN_HH_
