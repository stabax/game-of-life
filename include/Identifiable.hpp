//Identifiable.hpp

/*
*Cette interface est une base pour toutes
*les classes qui ont une sémentique d'entité
*ET qui doivent avoir un identifiant.
*/

#ifndef IDENTIFIABLE_HPP_
#define IDENTIFIABLE_HPP_

#include <string>

class Identifiable
{
  //raison de changement : Aucune

  //fonctions amies

public:
  //constructeurs
  Identifiable(std::string const& id);
  Identifiable(Identifiable const& Obj) = delete;
  Identifiable(Identifiable && Obj) = delete;

  //destructeur
  virtual ~Identifiable() = 0;

  //méthodes statiques et swap

  //accesseurs
  virtual std::string getId() const;

  //mutateurs

  //méthodes
  virtual std::string generateId() const = 0;
  bool isSameId(Identifiable const& Obj) const;

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)
  Identifiable& operator=(Identifiable const& Obj) = delete;

protected:
  //attributs
  std::string _id;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )

#endif //IDENTIFIABLE_HPP_
