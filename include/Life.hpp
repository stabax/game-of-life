//Life.hpp

/*
*Cette interface représente toutes
*les entitées vivantes possibles
*/

#ifndef LIFE_HPP_
#define LIFE_HPP_

#include "Placeable.hpp"
#include "Protein.hh"

#include <unordered_map>
#include <memory>


class Life : public Placeable
{
  //fonctions amies

public:
  //constructeurs
  Life(std::string const& id, std::weak_ptr<Slot> const& slot);

  //destructeur
  virtual ~Life() = 0;

  //méthodes statiques et swap

  //accesseurs

  //mutateurs

  //méthodes
  virtual void move() = 0;
  virtual void ingest() = 0;
  virtual Life* reproduce() = 0;

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs

};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //LIFE_HPP_
