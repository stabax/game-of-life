//Point.hh

#ifndef POINT_HH_
#define POINT_HH_

#include <functional>

struct Point
{
  Point(unsigned xx = 0, unsigned yy = 0);

  unsigned x;
  unsigned y;
};

//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )
bool operator==(Point const& A, Point const& B);
bool operator!=(Point const& A, Point const& B);


namespace std
{
  template<>
  class hash<Point>
  {
    public:
    size_t operator()(Point const& Obj) const
    {
        return hash<unsigned>()(Obj.x) ^ hash<unsigned>()(Obj.y);
    }
  };
}


#endif //POINT_HH_
