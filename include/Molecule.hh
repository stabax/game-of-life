//Molecule.hpp

#ifndef MOLECULE_HH_
#define MOLECULE_HH_

#include <unordered_map>
#include <tuple>
#include <string>


class Molecule
{
  //Raison de changement :

  //fonctions amies
  friend bool operator==(Molecule const& Obj1, Molecule const& Obj2);
  friend bool operator!=(Molecule const& Obj1, Molecule const& Obj2);
  friend std::hash<Molecule>;

public:
  //constructeurs
  Molecule(std::string const& name);
  //version appelée par les constructeurs de protein
  Molecule(std::string const& name, int temperature);
  Molecule(Molecule const& Obj);

  //destructeur
  virtual ~Molecule();

  //méthodes statiques et swap
  static void loadMoleculeFile(std::string const& path);

  //accesseurs
  std::string getName() const;

  //mutateurs

  //méthodes
  virtual std::string checkList(std::string const& name);

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  std::string _name;
  int _vaporizationTemperature;

  //tuple<0>=temperature de vaporisation
  static std::unordered_map<std::string, std::tuple<int>> moleculeList;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )
bool operator==(Molecule const& Obj1, Molecule const& Obj2);
bool operator!=(Molecule const& Obj1, Molecule const& Obj2);



namespace std
{
  template<>
  class hash<Molecule>
  {
    public:
    size_t operator()(Molecule const& Obj) const
    {
        return hash<string>()(Obj._name);
    }
  };
}


#endif //MOLECULE_HHH_
