//Cell.hh

#ifndef CELL_HH_
#define CELL_HH_

#include "Life.hpp"


class Cell : public Life
{
  //fonctions amies

public:
  //constructeurs
  Cell(std::unordered_map<Molecule, unsigned> const& moleculeList,
      std::unordered_map<Protein, unsigned> const& proteinList,
      std::weak_ptr<Slot> const& slot, std::string const& id = "");

  //destructeur
  virtual ~Cell();

  //méthodes statiques et swap

  //accesseurs

  //mutateurs

  //méthodes
  virtual std::string generateId() const;
  virtual Cell* reproduce();
  Cell* duplicate();
  virtual void move();
  virtual void ingest();

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  std::unordered_map<Molecule, unsigned> _moleculeList;
  std::unordered_map<Protein, unsigned> _proteinList;

};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )

#endif //CELL_HH_
