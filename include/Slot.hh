//Slot.hh

#ifndef SLOT_HH_
#define SLOT_HH_

#include <list>
#include <memory>

#include "Point.hh"
#include "Protein.hh"
#include "Virus.hh"
#include "Cell.hh"
#include "MultiCell.hh"
#include "json.hh"

class Terrain;

class Slot
{
  //fonctions amies

public:
  //constructeurs
  Slot(Terrain& terrain, Point const& position);
  Slot(Slot const& Obj) = delete;

  //destructeur
  virtual ~Slot();

  //méthodes statiques et swap

  //accesseurs
  Point getPosition() const;

  //mutateurs

  //méthodes
  //retourne true si il y a égal ou plus de amount sur le slot
  bool hasMolecule(unsigned amount) const;
  bool hasProtein(unsigned amount) const;
  bool hasLife(unsigned amount) const;


  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)
  Slot& operator=(Slot const& Obj) = delete;

protected:
  //attributs
  Terrain& _terrain;
  Point _position;

  std::unordered_map<Molecule, unsigned> _moleculeList;
  std::unordered_map<Protein, unsigned> _proteinList;
  std::list<std::shared_ptr<Cell>> _cellList;
  std::list<std::shared_ptr<Virus>> _virusList;
  std::list<std::shared_ptr<MultiCell>> _multiCellList;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //SLOT_HH_
