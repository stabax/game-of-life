//TerrainLoader.hh

/*
*
*
*
*/

#ifndef TERRAINLOADER_HH_
#define TERRAINLOADER_HH_

#include "Terrain.hh"
#include "json.hh"

class TerrainLoader
{
//raison de changement :

  //fonctions amies

public:
  //constructeurs
  protected:
  TerrainLoader() = delete;
  TerrainLoader(Terrain& terrain);

  //destructeur

  //méthodes statiques et swap
  public:
  static void load(std::string const& terrainPath, Terrain& terrain);

  //forward

  //accesseurs

  //mutateurs

  //méthodes
  protected:
  //charge les propriétés du terrain :
  void loadProperties(nlohmann::json const& j, std::string nodes);
  void loadSlotFile(nlohmann::json const& j, std::string nodes);
  //génère les molécules sur les slots :
  std::vector<std::tuple<Molecule, float, unsigned>> getSlotMoleculeInfo(nlohmann::json const& j, std::string nodes);
  //génère un slot
  std::shared_ptr<Slot> instantiateSlot(std::vector<std::tuple<Molecule, float, unsigned>> const& moleculeInfo, Point const& position);
  //place les slots sur le terrain dans les shapes définies
  void generateSlot(nlohmann::json const& j, std::string nodes);

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
  Terrain& _terrain;
  //on stock les json de chaque fichier de slot, afin de ne pas avoir
  //a re-parser ces fichiers à chaque fois que deux slots d'un même fichiers
  //sont appelés :
  std::unordered_map<std::string, nlohmann::json> _slotsJsonContainer;
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )

#endif //TERRAINLOADER_HH_
