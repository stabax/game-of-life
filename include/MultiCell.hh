//MultiCell.hh

#ifndef MULTICELL_HH_
#define MULTICELL_HH_

#include "Life.hpp"


class MultiCell : public Life
{
  //fonctions amies

public:
  //constructeurs

  //destructeur
  virtual ~MultiCell();

  //méthodes statiques et swap

  //accesseurs

  //mutateurs

  //méthodes

  //opérateurs méthodes ( =, (), [], ->, +=, -=, /=, *=, %=, ++, --)

protected:
  //attributs
};
//opérateurs non méthodes (+, -, *, /, %, ==, !=, <, >, <=, >=, <<, >> )


#endif //MULTICELL_HH_
