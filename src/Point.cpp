//Point.cpp

#include "Point.hh"

Point::Point(unsigned xx, unsigned yy) : 
x(xx),
y(yy)
{}


bool operator==(Point const& A, Point const& B)
{
  return ((A.x == B.x) && (A.y == B.y));
}


bool operator!=(Point const& A, Point const& B)
{
  return !(A == B);
}
