//TerrainLoader.cpp

#include <random>
#include <ctime>
#include <utility>
#include <fstream>
#include <array>

#include "TerrainLoader.hh"
#include "jsonHandler.h"

using json = nlohmann::json;


TerrainLoader::TerrainLoader(Terrain& terrain):
_terrain(terrain),
_slotsJsonContainer(std::unordered_map<std::string, json>())
{
}


void TerrainLoader::load(std::string const& terrainPath, Terrain& terrain)
{
  TerrainLoader loader(terrain);

  std::fstream file(terrainPath);
  if(! file)
    throw std::string("Cannot open file : " + terrainPath + ".");
  json j = json::parse(file);
  std::string nodes = "In file : " + terrainPath;

  //CHARGEMENT DES PROPERTIES
  loader.loadProperties(loadFromJson(j, "properties", nodes), nodes + getKeyBracket("properties"));

  //MISE EN MEMOIRE DES FICHIERS JSON NECESSAIRES
  json generateSlot = loadFromJson(j, "GenerateSlots", nodes);
  unsigned generateSlotSize = generateSlot.size();
  std::string generateSlotNodes = nodes + getKeyBracket("GenerateSlots");

  for(unsigned index = 0; index < generateSlotSize; ++index)
    loader.loadSlotFile(loadFromJson(generateSlot, index, generateSlotNodes), generateSlotNodes + getKeyBracket(index));
  loader.loadSlotFile(loadFromJson(j, "otherSlots", nodes), nodes + getKeyBracket("otherSlots"));

  //CHARGEMENT DES SLOTS
  for(unsigned index = 0; index < generateSlotSize; ++index)
    loader.generateSlot(loadFromJson(generateSlot, index, generateSlotNodes), generateSlotNodes + getKeyBracket(index));
  loader.generateSlot(loadFromJson(j, "otherSlots", nodes), nodes + getKeyBracket("otherSlots"));

}


void TerrainLoader::loadProperties(nlohmann::json const& j, std::string nodes)
{
  _terrain._dimensions.x = loadFromJson(j, "width", nodes);
  _terrain._dimensions.y = loadFromJson(j, "high", nodes);
  _terrain._temperature = loadFromJson(j, "temperature", nodes);
  _terrain._brightness = loadFromJson(j,  "brightness", nodes);
  _terrain._Xrays = loadFromJson(j, "Xrays", nodes);
}


void TerrainLoader::loadSlotFile(nlohmann::json const& j, std::string nodes)
{
  std::string slotName = loadFromJson(j, "slot", nodes);
  std::string slotPath = loadFromJson(j, "path", nodes);

  if(! _slotsJsonContainer.count(slotPath))
  {
    std::fstream file(slotPath);
    if(! file)
      throw std::string("Cannot open file : " + slotPath + ".");
    _slotsJsonContainer.emplace(slotPath, json::parse(file));
  }
}


void TerrainLoader::generateSlot(nlohmann::json const& j, std::string nodes)
{
  std::string slotName = loadFromJson(j, "slot", nodes);
  std::string slotPath = loadFromJson(j, "path", nodes);

  //CHARGEMENT DE L'OBJET SLOT QUE L'ON CHERCHE A GENERER
  std::string slotNodes = "In file : " + slotPath;
  json slot = loadFromJson(_slotsJsonContainer.at(slotPath), slotName, slotNodes);

  //CHERGEMENT DES INFOS SUR LES MOLECULES DU SLOT
  json moleculesField = loadFromJson(slot, "molecules", slotNodes + getKeyBracket(slotName));
  std::vector<std::tuple<Molecule, float, unsigned>> moleculeInfo(getSlotMoleculeInfo(moleculesField, slotNodes + getKeyBracket(slotName) + getKeyBracket("molecules")));

  //CHERGEMENT DES INFOS SUR LES PROTEINES DU SLOT

  //CHARGEMENT DU SHAPE
  json shape = loadFromJson(j, "shape", nodes);
  std::string shapeNodes = nodes + getKeyBracket("shape");
  std::string shapeName = loadFromJson(shape, "name", shapeNodes);

  //CHOIX DU SHAPE
  if(shapeName == "other")
  {
    for(unsigned x = 0; x < _terrain._dimensions.x; ++x)
    {
      for(unsigned y = 0; y < _terrain._dimensions.y; ++y)
      {
        Point pos(x,y);
        if(! _terrain._terrain.count(pos))
          _terrain._terrain.emplace(pos, instantiateSlot(moleculeInfo, pos));
      }
    }
  }
  else
  {
    std::string ifAlreadyExist = loadFromJson(j, "ifAlreadyExist", nodes);

    if(shapeName == "rectangle")
    {
      json from = loadFromJson(shape, "from", shapeNodes);
      json to = loadFromJson(shape, "to", shapeNodes);

      unsigned xBegin = loadFromJson(from, 0, shapeNodes + getKeyBracket("from"));
      unsigned xEnd = loadFromJson(to, 0, shapeNodes + getKeyBracket("to"));
      unsigned yBegin = loadFromJson(from, 1, shapeNodes + getKeyBracket("from"));
      unsigned yEnd = loadFromJson(to, 1, shapeNodes + getKeyBracket("to"));

      if(xBegin > xEnd)
        std::swap(xBegin, xEnd);
      if(yBegin > yEnd)
        std::swap(yBegin, yEnd);

      for(unsigned x = xBegin; x < xEnd && x < _terrain._dimensions.x; ++x)
      {
        for(unsigned y = yBegin; y < yEnd && y < _terrain._dimensions.y; ++y)
        {
          Point pos(x,y);
          _terrain._terrain.emplace(pos, instantiateSlot(moleculeInfo, pos));
        }
      }
    }
    else if(shapeName == "triangle")
    {

    }
    else if(shapeName == "circle")
    {

    }
    else
      throw std::string(shapeNodes + getKeyBracket("name") + " , this name is not allowed.");
  }
}


std::vector<std::tuple<Molecule, float, unsigned>> TerrainLoader::getSlotMoleculeInfo(json const& j, std::string nodes)
{
  std::vector<std::tuple<Molecule, float, unsigned>> moleculesInfo;

  unsigned size = j.size();
  for(unsigned index = 0; index < size; ++index)
  {
    json currentMolecule = loadFromJson(j, index, nodes);
    std::string localNodes = nodes + getKeyBracket(index);

    std::string name = loadFromJson(currentMolecule, "name", localNodes);
    float proba = loadFromJson(currentMolecule, "proba", localNodes);
    proba /= 100;
    unsigned max = loadFromJson(currentMolecule, "maxQuantity", localNodes);

    moleculesInfo.push_back(std::tuple<Molecule, float, unsigned>(name, proba, max));
  }
  return moleculesInfo;
}
#include <iostream>

std::shared_ptr<Slot> TerrainLoader::instantiateSlot(std::vector<std::tuple<Molecule, float, unsigned>> const& moleculeInfo, Point const& position)
{
  std::default_random_engine uniformGenerator(static_cast<unsigned>(time(nullptr)));
  std::unordered_map<Molecule, unsigned> moleculeList;

  for(std::tuple<Molecule, float, unsigned> const& info : moleculeInfo)
  {
    Molecule molecule(std::get<0>(info));
    moleculeList.emplace(molecule, 0);
    float proba = std::get<1>(info);
    unsigned max = std::get<2>(info);
    std::bernoulli_distribution bernoulli(proba);

    for(unsigned loop = 0; loop < max; ++loop)
    {
      uniformGenerator.seed(static_cast<unsigned>(time(nullptr)));
      std::cout<< uniformGenerator() << '\n';
      if(bernoulli(uniformGenerator))
        moleculeList.at(molecule) += 1;
    }
  }
  return std::make_shared<Slot>(_terrain, position);
}
