//Molecule.cpp

/*
*Cette classe à sémantique de valeur représente une molécule,
*à stocker sous forme d'effectif dans une std::unordered_map<Molecule, unsigned>
*/

#include "Molecule.hh"
#include "json.hh"

#include <fstream>

using json = nlohmann::json;

std::unordered_map<std::string, std::tuple<int>> Molecule::moleculeList = std::unordered_map<std::string, std::tuple<int>>();


Molecule::Molecule(std::string const& name):
_name(checkList(name)),
_vaporizationTemperature(std::get<0>(moleculeList.at(_name)))
{
}


Molecule::Molecule(std::string const& name, int temperature):
_name(name),
_vaporizationTemperature(temperature)
{
}


Molecule::Molecule(Molecule const& Obj):
_name(Obj._name),
_vaporizationTemperature(Obj._vaporizationTemperature)
{
}


Molecule::~Molecule()
{
}


void Molecule::loadMoleculeFile(std::string const& path)
{
  std::fstream file(path);
  json j = json::parse(file);
  j = j["molecule"];

  unsigned size = j.size();
  for(unsigned counter = 0; counter < size; ++counter)
  {
    if(j[counter]["name"].is_null())
      throw std::string("Name has not been declared in molecule definition file : " + path + ", field no" + std::to_string(counter) + ".");
    std::string name = j[counter]["name"];
    if(j[counter]["vaporizationTemperature"].is_null())
      throw std::string("vaporizationTemperature has not been declared in molecule definition file : " + path + ", field no" + std::to_string(counter) + "(named " + name +").");

    if(moleculeList.count(name))
      throw std::string("Multiple definition of molecule " + name +".");
    moleculeList.emplace(name, std::tuple<int>(j[counter]["vaporizationTemperature"]));
  }
}


std::string Molecule::getName() const
{
  return _name;
}


std::string Molecule::checkList(std::string const& name)
{
  return moleculeList.count(name) ? name : throw std::string("Molecule " + name + " has not been declared in any loaded molecule declaration file.");
}


bool operator==(Molecule const& Obj1, Molecule const& Obj2)
{
  if(Obj1._name == Obj2._name)
    return true;
  return false;
}


bool operator!=(Molecule const& Obj1, Molecule const& Obj2)
{
  if(Obj1._name != Obj2._name)
    return true;
  return false;
}
