//Protein.cpp

#include <fstream>

#include "protein.hh"
#include "json.hh"

using json = nlohmann::json;


std::unordered_map<std::string, std::tuple<int, unsigned>> Protein::proteinList = std::unordered_map<std::string, std::tuple<int, unsigned>>();


Protein::Protein(std::string const& name, unsigned usage):
Molecule(checkList(name), std::get<0>(proteinList.at(_name))),
_maxUsage(std::get<1>(proteinList.at(_name))),
_usage(usage),
_effects(std::unordered_map<std::string, unsigned>())
{
}


Protein::~Protein()
{
}


void loadProteinFile(std::string const& path)
{
  std::fstream file(path);
  json j = json::parse(file);

  for(unsigned counter = 0; ! j[counter].is_null(); ++counter)
  {}
}


std::string Protein::checkList(std::string const& name)
{
  return proteinList.count(name) ? name : throw std::string("Protein " + name + " has not been declared in data files.");
}


bool operator==(Protein const& Obj1, Protein const& Obj2)
{
  if(Obj1._name == Obj2._name && Obj1._usage == Obj2._usage)
    return true;
  return false;
}


bool operator!=(Protein const& Obj1, Protein const& Obj2)
{
  if(Obj1._name != Obj2._name || Obj1._usage != Obj2._usage)
    return true;
  return false;
}
