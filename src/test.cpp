//test.cpp

#include "test.h"
#include "Terrain.hh"

void test()
{
  Molecule::loadMoleculeFile("data/particles/molecules.json");
  Terrain terr("data/terrain/test_generator.json");

  std::cout << "x : " << terr.dimensions().x << '\n';
  std::cout << "y : " << terr.dimensions().y << '\n';

  for(unsigned x = 0; x < terr.dimensions().x; ++x)
  {
    for(unsigned y = 0; y < terr.dimensions().y; ++y)
    {
      if(terr.hasSlotMolecule(Point(x, y), 1))
        std::cout << 'x';
      else
        std::cout << ' ';
    }
    std::cout << '\n';
  }
}
