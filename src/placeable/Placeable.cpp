//Placeable.cpp

#include "Placeable.hpp"
#include "Slot.hh"


Placeable::Placeable(std::string const& id, std::weak_ptr<Slot> const& slot):
Identifiable(id),
_slot(slot)
{
}

Placeable::~Placeable()
{
}


Point Placeable::getPosition() const
{
  return _slot.lock()->getPosition();
}
