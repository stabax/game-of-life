//Cell.cpp

#include "Cell.hh"


Cell::Cell(std::unordered_map<Molecule, unsigned> const& moleculeList,
          std::unordered_map<Protein, unsigned> const& proteinList,
          std::weak_ptr<Slot> const& slot, std::string const& id):
Life(id.size() != 0 ? id : generateId(), slot),
_moleculeList(moleculeList),
_proteinList(proteinList)
{
}


Cell::~Cell()
{
}


std::string Cell::generateId() const
{
  return "tempID";
}


Cell* Cell::reproduce()
{
  return duplicate();
}


Cell* Cell::duplicate()
{
  return new Cell(_moleculeList, _proteinList, _slot);
}


void Cell::move()
{

}


void Cell::ingest()
{

}
