//main.cpp

#include <unordered_set>
#include <string>
#include <iostream>
#include <exception>

#include "main_functions.h"

int main(int argc, char* argv[])
{
  try
  {
    std::unordered_set<std::string> args;
    for(int count = 1; count < argc; ++count)
      args.emplace(argv[count]);

    if(args.count("--help") || args.count("-h"))
    {
      help();
      args.erase("--help");
      args.erase("-h");
    }
    if(args.count("--version") || args.count("-v"))
    {
      version();
      args.erase("--version");
      args.erase("-v");
    }
    if(args.count("-nogui"))
    {
      mainNoGUIPlay();
      args.erase("-nogui");
      args.erase("-test");
    }
    else if(args.count("-test"))
    {
      mainTest();
      args.erase("-test");
    }
    if(! args.empty())
    {
      std::cout << "----------------------------------\n";
      std::cout << "These arguments are invalid :\n\n";
      invalideArgs(args);
      std::cout << "\nplease try < --help > for more informations.\n";
      std::cout << "----------------------------------\n";
    }
    else
    {
      mainPlay();
    }
  }
  catch(std::exception const& e)
  {
    std::cout << "\n" << e.what() << "\n";
  }
  catch(std::string const& e)
  {
    std::cout << "\n" << e << "\n";
  }
  catch(...)
  {
    std::cout << "\nUnknown exception thrown\n";
  }
return 0;
}
