//Terrain.cpp

#include "Terrain.hh"
#include "TerrainLoader.hh"


Terrain::Terrain():
_temperature(0),
_brightness(0),
_Xrays(0),
_dimensions(Point()),
_terrain(std::unordered_map<Point, std::shared_ptr<Slot>>()),
_gas(std::unordered_map<Molecule, unsigned>())
{
}


Terrain::Terrain(int temperature,
        unsigned brightness,
        unsigned Xrays,
        Point const& dimensions,
        std::unordered_map<Point, std::shared_ptr<Slot>> terrain,
        std::unordered_map<Molecule, unsigned> gas):
_temperature(temperature),
_brightness(brightness),
_Xrays(Xrays),
_dimensions(dimensions),
_terrain(terrain),
_gas(gas)
{
}


Terrain::Terrain(std::string const& terrainPath):
Terrain()
{
  TerrainLoader::load(terrainPath, *this);
}


Terrain::~Terrain()
{
}


Point const& Terrain::dimensions() const
{
  return _dimensions;
}


std::tuple<int, unsigned, unsigned> const Terrain::getProperties() const
{
  return std::tuple<int, unsigned, unsigned>(_temperature, _brightness, _Xrays);
}


std::unordered_map<Molecule, unsigned> const& Terrain::getGas() const
{
  return _gas;
}


bool Terrain::hasSlotMolecule(Point const& pos, unsigned amount) const
{
  return _terrain.at(pos)->hasMolecule(amount);
}


bool Terrain::hasSlotProtein(Point const& pos, unsigned amount) const
{
  return _terrain.at(pos)->hasProtein(amount);
}


bool Terrain::hasSlotLife(Point const& pos, unsigned amount) const
{
  return _terrain.at(pos)->hasLife(amount);
}
