//Slot.cpp

#include "Slot.hh"

using json = nlohmann::json;


Slot::Slot(Terrain& terrain, Point const& position):
_terrain(terrain),
_position(position),
_moleculeList(std::unordered_map<Molecule, unsigned>()),
_proteinList(std::unordered_map<Protein, unsigned>()),
_cellList(std::list<std::shared_ptr<Cell>>()),
_virusList(std::list<std::shared_ptr<Virus>>()),
_multiCellList(std::list<std::shared_ptr<MultiCell>>())
{
}


Slot::~Slot()
{
}


Point Slot::getPosition() const
{
  return _position;
}


bool Slot::hasMolecule(unsigned amount) const
{
  unsigned onSlot = 0;
  for(auto i : _moleculeList)
    onSlot += i.second;
  return onSlot > amount ? true : false;
}


bool Slot::hasProtein(unsigned amount) const
{
  unsigned onSlot = 0;
  for(auto i : _proteinList)
    onSlot += i.second;
  return onSlot > amount ? true : false;
}


bool Slot::hasLife(unsigned amount) const
{
  unsigned onSlot = _cellList.size() +_virusList.size() +  _multiCellList.size();
  return onSlot > amount ? true : false;
}
