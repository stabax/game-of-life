//Identifiable.cpp

#include "Identifiable.hpp"


Identifiable::Identifiable()
{
}


Identifiable::~Identifiable()
{
}


bool Identifiable::isSameId(Identifiable const& Obj) const
{
	return getId() == Obj.getId();
}
