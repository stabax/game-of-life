//Identifiable.cpp

#include "Identifiable.hpp"


Identifiable::Identifiable(std::string const& id):
_id(id)
{
}

Identifiable::~Identifiable()
{
}

std::string Identifiable::getId() const
{
  return _id;
}


bool Identifiable::isSameId(Identifiable const& Obj) const
{
  return _id == Obj._id;
}
