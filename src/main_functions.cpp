//main_functions.cpp

#include "main_functions.h"
#include "test.h"
#include "play.h"

void clear(unsigned lines)
{
  for(unsigned count = 0; count < lines; ++lines)
    std::cout << '\n';
}


void version()
{
  std::cout << "Game Of Life\n";
  std::cout << "Developed by Baxlan\n";
  std::cout << "Version: wip 0.0.0\n";
  std::cout << "Distributed on 11 JUL 2017\n";
  std::cout << "Project started on 11 JUL 2017\n";
  std::cout << "Stabax Ltd. 2013.\n";
}


void help()
{
  std::cout << "\n\nGame Of Life :\n";
  std::cout << "cell life and evolution simulator.\n";
  std::cout << "This is a game where the player can embody a celle,\n";
  std::cout << "control a box of dough, or just monitor the cell evolution.\n\n";
  std::cout << "Usage: Game-Of-Life [options]\n\n";
  std::cout << " --help | -h      = Prompt this message.\n";
  std::cout << " --version | -v   = Prompt development informations.\n";
  std::cout << " -test            = Launch a test version for debugging.\n";
  std::cout << " -nogui           = Start game in console mode.\n";
}


void invalideArgs(std::unordered_set<std::string>& args)
{
  std::unordered_set<std::string>::iterator end = args.end();
  for(std::unordered_set<std::string>::iterator it = args.begin(); it != end; it++)
    std::cout << *it << "\n";
}


void mainPlay()
{
	play();
}


void mainNoGUIPlay()
{
	noGUIPlay();
}


void mainTest()
{
  test();
}
