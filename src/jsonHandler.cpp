//jsonHandler.cpp

#include "jsonHandler.h"


std::string getKeyBracket(std::string const& key)
{
  return "[\"" + key + "\"]";
}


std::string getKeyBracket(unsigned key)
{
  return '[' + std::to_string(key) + ']';
}


nlohmann::json loadFromJson(nlohmann::json const& j, std::string const& key, std::string const& nodes)
{
  std::string bracketKey(getKeyBracket(key));
  if(j.is_array())
    throw nodes + " : shouldn't be an array.";
  if(! j.count(key))
    throw nodes + bracketKey + " : has not been defined.";
  return j.at(key);
}


nlohmann::json loadFromJson(nlohmann::json const& j, unsigned key, std::string const& nodes)
{
  std::string bracketKey(getKeyBracket(key));
  if(! j.is_array())
    throw nodes + " : should be an array.";
  if(! (key < j.size()))
    throw nodes + bracketKey + " : has not been defined.";
  return j.at(key);
}
