
            **************************
            *                        *
            *   GOL : GAME OF LIFE   *
            *   cahier des charges   *
            *                        *
            **************************

EN UNE PHRASE : GAME OF LIFE
-Le but est d'observer et d'influencer le dévelopement de cellules dans une boite de petri.


MODES DE JEU :

  -mode Evolution : mode de jeu de base, avec objectifs
-Le but est d'observer et d'influencer le dévelopement de cellules dans une boite de petri, dit "terrain".
-La progression du joueur se fait par niveaux, dit "phases". Chaque phase propose divers objectifs à accomplir pour passer à la phase suivante. (Exemples: x cellules sur le terrain, x cellule d'un type donné sur le terrain, faire survivre x cellules d'un type donné pendant x ticks, tuer x cellules de ce type... etc).
-Le terrain commence de petite taille (5x5 par ex) et sera agrandit lors de certains changements de phase.
-Lors de certaines phases, les conditions du terrain (température, luminosité...) seront imposées au joueur tel un handicap. Lors d'autres phases, le joueur pourra faire varier lui même ces conditions.
-Lors de certaines phases, le joueur aura des bactéries et/ou des items qu'il pourra disposer sur le terrain.

  -mode domination : 


  -mode Laboratoire : 


  -mode "plant vs zombies": des cellules ennemies fonce du coté du joueur. Les cellules du joueur doivent se défendre en émettant des produits toxics ou en mangeant les ennemis


  -mode multi : plusieurs joueurs s'affrontent dans une arène et s'entreBouffent.










LE TERRAIN ET LES SLOT















LES ELEMENTS
Il y a 3 types d'éléments : les gaz, les molécules et les protéines.
-Les éléments sont des particules, pouvant se trouver dans un life ou non, et pouvant etre produite naturellement par réaction, par synthèse dans une cellule, ou ne pouvant pas etre créée du tout (présente en début de partie seulement).
-Le point commun entre les éléments est qu'il peuvent tous interagir entre eux pour former de nouveaux éléments.
-exemples : (6 eau + 6 CO2 + calvinase + lumière = 1 glucose + 6 02, selon luminosité, de la qualité des pigments absorbeurs de lumière, de du chloroplastase)   (1 glucose + 6 O2 + krebsase + 32ADP + 32 phosphate = 6 CO2 + 6 eau + 32ATP, le nombre d'ATP dépendant de la qualité de la krebstase).


LES GAZ
-un gaz est une particule n'appartenant pas à un slot particulier : elle appartient à l'air ambiant et appartient donc au terrain (si elle n'est pas dans un etre vivant). De ce fait, tous les slots ont accès aux memes gaz et se partagent ces ressources.
-Du méthane, de l'ammoniac, du dihydrogène et des molécules d'eau excitées avec des Xrays permettent de produire des composés organiques basiques (acides aminés, sucres, base azotée). L'eau n'étant pas un gaz, ces produits se trouvent sur les slots où il y avait de l'eau.
-le dioxygène est une ressource partagée entre tous les vivants, puisque c'est un gaz.


LES MOLECULES
-Une molécule est une particule appartenant à un slot (si elle n'est pas dans un etre vivant). Seules ces deux entitées sont y ont donc accès.
-les bases (bases azotées) sont l'un des deux composants des nucléotides (avec le sucre). La base 1 est également un composé de l'ATP (avec le phosphate)
-Une molécule de type nucléotide dans une cellule sert "d'argent", ou de points, pouvant etre dépensés pour débloquer des évolutions. Le ribonucléotide 1 vaut 1 point, le ribonucléotide 2 vaut 2 points, le désoxyribonucléodite 1 vaut 4 points, et le désoxyribonucléotide 2 vaut 6 points.
-Une molécule de type acide aminé est le constituant élémentaire des protéines synthetisées par une cellule. Débloquer les différents tiers d'acide aminé permet de produire de nouvelles protéines, débloquant potetiellement de nouvelles réactions ou de nouveaux comportements.


LES PROTEINES
-Une protéine ne peut etre produite QUE par synthèse dans une cellule.
-Une protéine est une particule, et plus précisément une molécule, (avec les memes propriétés que ci dessus) en plus de nouvelles propriétés.
-Une protéine est composée de plusieurs molécules de nom "acide aminé". Chaque protéine est définie par le nombre et les "types" (1, 2, 3... des sortes de tier) d'acides aminés qui la composent.
-Une protéine a des caracteristiques particulières : elle a une durée de vie limitée (elle s'use a force d'etre utilisée et fini par devenir inactive, elle peut etre réactivée ou décomposée en acides aminés par une autre protéine, consommant ATP)
-Une protéine peut : réaliser certaines réactions (enzyme), structurer/réparer la cellule (noyau, cytosquelette) (structure), communiquer avec d'autres cellules (hormone), attaquer à distance (toxines), attaquer au corp à corp (perforine, pics), détecter des signaux environementaux ou hormonaux (recepteurs), permettre de se déplacer (slime, flagel)(moteur)...
-Une protéine désignée par une lettre (a, b, c ...) désigne l'efficacité avec laquelle elle effectue son travail. La désignation par un chiffre (1, 2 3...) indique le tier, c'est a dire le niveau "technologique" des reactions qu'elle peut induire. (Une "krebstase c" produit en quantité de l'ATP, une "aminase 3" produit des acides aminées de type 3, technologiquement élevés).










LES ETRES VIVANTS (life)


LES CELLULES

-La base d'une cellule est son égergie : à chaque déplacement (donc à chaque tick) elle perd de l'énergie. Si l'energie vaut 0, la cellule meurt. Si l'energie atteint un seuil, la cellule se duplique.
-Une cellule se déplace. Lorsqu'une cellule se déplace sur une case où il y a un ELEMENT, elle le mange et bénéficie de ses effets.
-Les cellules peuvent se pimper (ENHANCE) via certains items : les STUFF (exemple : s'enduire de slime, se recouvrire d'épines...).
-Deux cellules sur la même case se combattent : celle qui a le plus de level mange l'autre (si les niveaux sot éguaux et qu'aucun équipable ou effet n'entre en jeu, alors la cellule la plus vieille gagne)), et celle qui mange l'autre voit son niveau augmenter.
-Il y a des cellules actives et passives : les passives ne survivent que grâce aux consumables du terrain (donc leur niveau est toujours égal à 0). Si deux passives sont sur la même case, c'est la plus vieille (ou celle qui a des capacités spéciales) qui mangent les consumables de la case.
-Il existe des cellules intelligentes ou non (avec des algorithmes se déplaçant vers les consommables ou d'autres cellules, et détectent si ladite cellule est dangereuse ou non).
-Les déplacements peuvent consommer plus ou moins d'énergie selon certaines spécificités.

-production d'ATP (énergie) : 1 glucose + 6 O2 + krebsase + 32ADP + 32 phosphate = 6 CO2 + 6 eau + 32ATP, le rapport nombre d'ATP dépendant de la qualité de la krebstase
-il faut une enzyme différente pour découper chaque polyoside différent. Il faut une enzyme pour transformer chaque ose en glucose
-les nucléotides (bases azotees) absorbées/produites permettent de débloquer des évolutions (nouvelle protéine synthetisable = nouvelles reactions possibles). Il faut voir les nuclotides comme de l'argent pour débloquer la suite.


LES VIRUS


LES MULTICELL



DEROULEMENT D'UN TICK

-le joueur peut interagir
-réactions les Item du terrain et les items apportés par le joueur
-confrontation des cellules et des bactéries apportées par le joueur
-les cellules survivantes mangent les consumables créés par les réactions précédentes
-déplacement des cellules
-confrontation des cellules
-les cellules survivantes mangent les consumables sur la case
-les cellules se dupliquent si elles le peuvent
-confrontation des cellules dupliquées
-les cellules vieillsent d'un tick



