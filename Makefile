CC = g++

RM = rm -rf

CXXFLAGS = -g -std=c++11 -pthread -Wall -Wextra -Wunused-macros -Wshadow -Wundef -pedantic -Wpointer-arith -Wcast-qual -Wcast-align -Wold-style-cast -Wconversion -Wsign-conversion -Wdouble-promotion -Wfloat-equal -Woverloaded-virtual -Weffc++ -Wswitch-default -Werror -O2 -Os -Wl,--no-as-needed -I$(INCDIR)

#-s annule -g
#-Wunreachable-code ==> warning si un bout de code n'est jamais exétuté
#-Wdisabled-optimization ===> warning si le compilateur n'a pas réussi a optimiser un bout de code trop compliqué
#-m8-bit -m16-bit -m32-bit ===> l'alignement des variables se fait sur 8, 16 ou 32 bits (32 par défaut)
#-flto ===> supprime les erreurs de vtable quand des méthodes virtuelles sont déclarées(.h) mais non implémentées(.cpp) (vtable = VMT = virtual method table)
#-DNDEBUG désactive les assetions

NAME = Game-Of-Life

BINDIR = bin

SRCDIR = src

INCDIR = include

SRCS =  $(SRCDIR)/main.cpp \
  $(SRCDIR)/main_functions.cpp \
	$(SRCDIR)/play.cpp \
	$(SRCDIR)/test.cpp \
  $(SRCDIR)/Point.cpp \
	$(SRCDIR)/jsonHandler.cpp \
  $(SRCDIR)/Identifiable.cpp \
  $(SRCDIR)/gameplay/Player.cpp \
  $(SRCDIR)/gameplay/Bot.cpp \
  $(SRCDIR)/gameplay/Game.cpp \
  $(SRCDIR)/gameplay/Profile.cpp \
  $(SRCDIR)/gameplay/Terrain.cpp \
  $(SRCDIR)/gameplay/Inventory.cpp \
  $(SRCDIR)/gameplay/Slot.cpp \
  $(SRCDIR)/Placeable/Placeable.cpp \
  $(SRCDIR)/Placeable/life/Life.cpp \
  $(SRCDIR)/Placeable/life/Cell.cpp \
  $(SRCDIR)/Placeable/life/MultiCell.cpp \
  $(SRCDIR)/Placeable/life/Virus.cpp \
  $(SRCDIR)/particles/Molecule.cpp \
  $(SRCDIR)/particles/Protein.cpp	\
	$(SRCDIR)/loader/TerrainLoader.cpp

OBJS = $(SRCS:.cpp=.o)


all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(OBJS) -o $(BINDIR)/$(NAME) $(CXXFLAGS)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
